package tpcompiladores;

import java.util.ArrayList;
import java.util.Collections;

public class TranslatorService {

    private static final String ALPHABET = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ,";
    private static final String VOWELS = "aeiouAEIOU";
    private char[] inputString;
    private int currentIndex;
    private char currentInput;

    public TranslatorService(char[] inputString) {
        this.inputString = inputString;
        this.currentInput = inputString[0];
    }

    private void match(char expectedInput) {
        if (currentInput == expectedInput) {
            getNextInput();
        } else if (ALPHABET.indexOf(currentInput) == -1 ) {
            throw new RuntimeException(("La entrada actual no pertenece al alfabeto: " + currentInput));
        } else {
            throw new RuntimeException(("Entrada actual no es igual a la esperada: " + currentInput));
        }
    }

    private void getNextInput() {
        if (currentIndex >= inputString.length) {
            currentInput = '$';
        }
        while(currentIndex < inputString.length){
            if(inputString[currentIndex] == ' '){
                currentIndex++;
            }else {
                currentInput = inputString[currentIndex];
                currentIndex++;
                break;
            }
        }

    }

    //Método correspondiente a "letra"
    private char isALetter() {
        if (Character.isLetter(currentInput)) {
            match(currentInput);
            return currentInput;
        } else {
            if (currentInput == '$') {
                return Character.MIN_VALUE;
            }
            throw new RuntimeException("No se encontró una letra: " + currentInput);
        }
    }

    private String word() {
        ArrayList<Character> letters = new ArrayList<>();
        while (currentInput != ',' && currentInput != '$') {
            char letterToAdd = isALetter();
            if (letterToAdd != '$' && letterToAdd != ',') {
                letters.add(letterToAdd);
            }
        }
        char lastCharOfArray = letters.get(letters.size() - 1);
        if(lastCharOfArray == '$'){
            lastCharOfArray = letters.get(letters.size() - 2);
        }
        if (VOWELS.indexOf(lastCharOfArray) != -1) {
            Collections.reverse(letters);
            return arrayToString(letters);
        }
        return "";
    }

    private String arrayToString(ArrayList<Character> letters) {
        StringBuilder result = new StringBuilder(letters.size());
        for (Character c : letters) {
            result.append(c);
        }
        return result.toString();
    }

    private String listOfWords() {
        if (currentInput == ',') {
            match(',');
            currentIndex--;
            String firstWord = word();
            String restOfWords = listOfWords();
            return firstWord + " " + restOfWords;
        } else {
            return " ";
        }
    }

    public String begin() {
        String firstWord = word();
        String restOfWords = listOfWords();
        return firstWord + " " + restOfWords;
    }

}
