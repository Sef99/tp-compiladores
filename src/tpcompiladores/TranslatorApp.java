package tpcompiladores;

public class TranslatorApp {

    public static final String INPUT_STRING = "hola, chau, bonbon, auto, autista, pared, porton";

    public static void main(String[] args) {
        char[] inputString = INPUT_STRING.toCharArray();
        TranslatorService translatorService = new TranslatorService(inputString);
        String result = translatorService.begin();
        System.out.println(result);
    }

}
